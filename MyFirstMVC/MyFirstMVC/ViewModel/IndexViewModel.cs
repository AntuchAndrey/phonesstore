﻿using MyFirstMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyFirstMVC.ViewModel
{
    public class IndexViewModel
    {
        public PageViewModel PageViewModel { get; set; }

        public IEnumerable<Phone> Phones { get; set; }

        public IEnumerable<Company> Companies { get; set; }

        public IEnumerable<Category> Categories { get; set; }



        public Company Company { get; set; }
        public Category Category { get; set; }
        public string Name { get; set; }
        public int? PriceFrom { get; set; }
        public int? PriceTo { get; set; }
        public bool? Exist { get; set; }
    }
}

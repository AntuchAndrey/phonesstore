﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyFirstMVC.Models;
using MyFirstMVC.ViewModel;

namespace MyFirstMVC.Controllers
{
    public class PhoneController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _environment;

        public PhoneController(ApplicationDbContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        public enum SortPhones
        {
            NameAsc,
            NameDesc,
            PriceAsc,
            PriceDesc,
            CategoryAsc,
            CategoryDesc,
            CompanyAsc,
            CompanyDesc,
            RaitingAsc,
            RaitingDesc
        }



        // GET: Phone
        public ActionResult Index(int? companyId, string Name, int? PriceFrom, int? PriceTo, bool? Exist, int? categoryId, SortPhones sortPhones = SortPhones.NameAsc, int page = 1)
        {
            List<Company> companies = _context.Companies.ToList();
            List<Category> categories = _context.Categories.ToList();


            IQueryable<Phone> phones = _context.Phones.Include(x => x.Company).Include(x => x.Category).OrderBy(x => x.Name);

            IndexViewModel ivm = new IndexViewModel();

            if (companyId.HasValue)
            {
                phones = phones.Where(p => p.Company.Id == companyId.Value);
                ivm.Company = companies.FirstOrDefault(c => c.Id == companyId.Value);
            }

            if(!string.IsNullOrWhiteSpace(Name))
            {
                phones = phones.Where(p => p.Name.Contains(Name, StringComparison.OrdinalIgnoreCase));
                ivm.Name = Name;
            }

            if(PriceFrom.HasValue)
            {
                phones = phones.Where(p => p.Price >= PriceFrom);
                ivm.PriceFrom = PriceFrom;
            }

            if (PriceTo.HasValue)
            {
                phones = phones.Where(p => p.Price <= PriceTo);
                ivm.PriceTo = PriceTo;
            }
            if (categoryId.HasValue)
            {
                if (categoryId != -1)
                {
                    phones = phones.Where(p => p.Category.Id == categoryId.Value);
                    ivm.Category = categories.FirstOrDefault(c => c.Id == categoryId.Value);
                }
                
            }

            if (Exist != false && Exist != null)
            {
                phones = phones.Where(p => p.Available == true);
                ivm.Exist = Exist;
            }

            
            
            ViewBag.Message = _context.Currencies;

            ViewBag.NameSort = sortPhones == SortPhones.NameAsc ? SortPhones.NameDesc : SortPhones.NameAsc;
            ViewBag.PriceSort = sortPhones == SortPhones.PriceAsc ? SortPhones.PriceDesc : SortPhones.PriceAsc;
            ViewBag.CategorySort = sortPhones == SortPhones.CategoryAsc ? SortPhones.CategoryDesc : SortPhones.CategoryAsc;
            ViewBag.CompanySort = sortPhones == SortPhones.CompanyAsc ? SortPhones.CompanyDesc : SortPhones.CompanyAsc;
            ViewBag.RaitingSort = sortPhones == SortPhones.RaitingAsc ? SortPhones.RaitingDesc : SortPhones.RaitingAsc;

            switch (sortPhones)
            {
                case SortPhones.NameAsc:
                    //ViewBag.NameSort = SortPhones.NameDesc;
                    phones = phones.OrderBy(p => p.Name);
                    break;
                case SortPhones.NameDesc:
                    phones = phones.OrderByDescending(p => p.Name);
                    break;
                case SortPhones.PriceAsc:
                    // ViewBag.PriceSort = SortPhones.PriceDesc;
                    phones = phones.OrderBy(p => p.Price);
                    break;
                case SortPhones.PriceDesc:
                    phones = phones.OrderByDescending(p => p.Price);
                    break;
                case SortPhones.CategoryAsc:
                    //ViewBag.CategorySort = SortPhones.CompanyDesc;
                    phones = phones.OrderBy(c => c.Category.Name);
                    break;
                case SortPhones.CategoryDesc:
                    phones = phones.OrderByDescending(c => c.Category.Name);
                    break;
                case SortPhones.CompanyAsc:
                    // ViewBag.CompanySort = SortPhones.CompanyDesc;
                    phones = phones.OrderBy(c => c.Company.Name);
                    break;
                case SortPhones.CompanyDesc:
                    phones = phones.OrderByDescending(c => c.Company.Name);
                    break;
                case SortPhones.RaitingAsc:
                    // ViewBag.CompanySort = SortPhones.CompanyDesc;
                    phones = phones.OrderBy(c => c.Average);
                    break;
                case SortPhones.RaitingDesc:
                    phones = phones.OrderByDescending(c => c.Average);
                    break;
            }
            //
            int pageSize = 3;
            //IQueryable<User> source = db.Users.Include(x => x.Company);
            var count = phones.Count();
            ivm.Phones = phones.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            ivm.PageViewModel = pageViewModel;
            

            ivm.Companies = companies;
            //ivm.Phones = phones;
            ivm.Categories = categories;
            
            foreach (var phone in ivm.Phones)
            {
                var reviews = _context.Reviews.Where(x => x.PhoneId == phone.Id).Select(p => p.Rating).ToArray();
                if (reviews.Length != 0)
                {
                    phone.Average = reviews.Average();
                }
                
            }

            return View(ivm);
        }

        public IActionResult Download(int id)
        {
            try
            {
                Phone phone = _context.Phones.FirstOrDefault(x => x.Id == id);
                string filePath = Path.Combine(_environment.ContentRootPath, $"InfoPhoneFile/Phone_{phone.Name}.txt");
                string fileType = "application/txt";
                string fileName = $"{phone.Name}.txt";
                if (!System.IO.File.Exists(filePath))
                {
                    throw new FileNotFoundException($"File Not Found by Phone {phone.Name}");
                }
                return PhysicalFile(filePath, fileType, fileName);
            }
            catch (Exception e)
            {
                ViewData["Message"] = e.Message;
                return View("404");
            }

        }
        // GET: Phone/Details/5
        public ActionResult Details(int id)
        {
            Phone phone = _context.Phones.Include(x => x.Company).Include(x => x.Category).FirstOrDefault(x => x.Id == id);
            if (phone == null)
            {
                return NotFound($"Сорян, телефона с id {id} нету");
            }
            ViewBag.Message = _context.Currencies;
            List<Reviews> reviews = _context.Reviews.Where(x => x.PhoneId == id).ToList();
            ViewData["Reviews"] = reviews;
            return View(phone);
        }

        
        public IActionResult Redirect(int id)
        {
            Phone phone = _context.Phones.FirstOrDefault(x => x.Id == id);
            string url = String.Empty;
            string IPhoneSite = "https://www.apple.com/ru/iphone/";
            string SonySite = "https://www.sony.ru/";
            if (phone.Name == "Iphone 4")
            {
                url = IPhoneSite;
            }
            else if(phone.Name == "Xperia")
            {
                url = SonySite;
            }
            return Redirect(url);
        }


        // GET: Phone/Create
        public ActionResult Create()
        {
            ViewData["Companies"] = _context.Companies.ToList();
            ViewData["Categories"] = _context.Categories.ToList();
            return View();
        }

        // POST: Phone/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Phone phone)
        {
            try
            {
                // TODO: Add insert logic here
                _context.Phones.Add(phone);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Phone/Edit/5
        public ActionResult Edit(int id)
        {
            Phone phone = _context.Phones.Include(x => x.Company).Include(x => x.Category).FirstOrDefault(x => x.Id == id);
            //Phone phone = _context.Phones.Include(x => x.CategoryId).FirstOrDefault(x => x.Id == id);
            ViewData["Companies"] = _context.Companies.ToList();
            ViewData["Categories"] = _context.Categories.ToList();
            return View(phone);
        }

        // POST: Phone/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Phone phone)
        {
            try
            {
                _context.Update(phone);
                _context.SaveChanges();
            }
            catch
            {
                return NotFound();
            }
            return RedirectToAction(nameof(Index));
        }

        // GET: Phone/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Phone/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
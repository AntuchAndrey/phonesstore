﻿using System;
using System.Collections.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyFirstMVC.Models;
using Microsoft.AspNetCore.Hosting;

namespace MyFirstMVC.Controllers
{
    public class ValidationController : Controller
    {

        private readonly ApplicationDbContext _context;
        private List<Company> companies;
        
        public ValidationController(ApplicationDbContext context, IHostingEnvironment environment)
        {
            _context = context;
            companies = _context.Companies.ToList();
        }


        [AcceptVerbs("Get")]
        public IActionResult CheckCompany(string Name)
        {
            foreach (var comp in companies)
            {
                if (Name.Contains(comp.Name, StringComparison.CurrentCultureIgnoreCase))
                {
                    return Json(false);
                }
            }

            return Json(true);
        }

        [AcceptVerbs("Get", "Post")]
        public ActionResult ValidateName(String name)
        {
            String[] arr = new string[] { "тест", "тест2" };

            if (arr.Any(b => name.Contains(b, StringComparison.CurrentCultureIgnoreCase)))
            {
                return Json(false);
            }

            return Json(true);
        }


        //[AcceptVerbs("Get")]
        //public IActionResult ValidateName(string name)
        //{
        //    if (BadWords.Any(b => name.Contains(b, StringComparison.OrdinalIgnoreCase)))
        //        return Json(false);

        //    return Json(true);
        //}
    }
}
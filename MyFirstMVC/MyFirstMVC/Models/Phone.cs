﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyFirstMVC.Models
{
    public class Phone
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "укажи название телефона")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "длина названия от 1 до 50")]
        public string Name { get; set; }

        [Required]
        [Range(1, 100000, ErrorMessage = "Недопустимое значение Цены")]
        public double Price { get; set; }

        [Required]
        public bool? Available { get; set; }

        [Required]
        public int CategoryId { get; set; }
        public Category Category { get; set; }

        [Required]
        public int CompanyId { get; set; }
        public Company Company { get; set; }

        public double? Average { get; set; }

        public IEnumerable<Order> Orders { get; set; }

        public IEnumerable<PhoneOnStock> Stocks { get; set; }

        public IEnumerable<Reviews> Reviews { get; set; }
    }

    

}

﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyFirstMVC.Models
{
    public class Company
    {
        public int Id { get; set; }

        [Required]
        [Remote(action: "CheckCompany", controller: "Validation", ErrorMessage = "This company already exists")]
        public string Name { get; set; }
        
        [Required]
        public string Email { get; set; }

        [Required]
        public DateTime CreateData { get; set; }

        
        public IEnumerable<Phone> Phones { get; set; }
    }
}

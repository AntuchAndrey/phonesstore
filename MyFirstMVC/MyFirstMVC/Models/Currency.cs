﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyFirstMVC.Models
{
    public class Currency
    {
        public string Id { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencyName { get; set; }
        public double CurrencyRate { get; set; } //стоимость 1 доллара в текущей валюте
        
    }
}

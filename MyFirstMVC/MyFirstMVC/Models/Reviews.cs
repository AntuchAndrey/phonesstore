﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyFirstMVC.Models
{
    public class Reviews
    {
        public int Id { get; set; }

        [Required]
        public int Rating { get; set; }

        [Required(ErrorMessage = "укажи коментарий")]
        [StringLength(1000, MinimumLength = 1, ErrorMessage = "длина названия от 1 до 50")]
        public string Review { get; set; }

        [Required(ErrorMessage = "укажи ваше имя")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "длина названия от 1 до 50")]
        public string NamePersonReview { get; set; }

        [Required]
        public int PhoneId { get; set; }
        public Phone Phone { get; set; }

    }
}

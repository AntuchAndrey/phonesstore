﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyFirstMVC.Migrations
{
    public partial class AddEmailAndCreateDataInCompanies : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "CreateData",
                table: "Companies",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Companies",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreateData", "Email" },
                values: new object[] { new DateTime(1976, 4, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.apple.com/" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreateData", "Email" },
                values: new object[] { new DateTime(1938, 3, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.samsung.com/us/" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreateData", "Email" },
                values: new object[] { new DateTime(1865, 5, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.nokia.com/ru_int" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreateData", "Email" },
                values: new object[] { new DateTime(2010, 4, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.mi.com/en/index.html" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreateData", "Email" },
                values: new object[] { new DateTime(2003, 3, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.meizu.com/" });

            migrationBuilder.UpdateData(
                table: "Companies",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreateData", "Email" },
                values: new object[] { new DateTime(1946, 5, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "https://www.sony.com/" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreateData",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Companies");
        }
    }
}

﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MyFirstMVC.Models;

namespace MyFirstMVC.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20180925074539_AddMorePhonesAndCategoriesAndCompaniesAndAddViewModelPlusSomeChengesInPhonesIndexView")]
    partial class AddMorePhonesAndCategoriesAndCompaniesAndAddViewModelPlusSomeChengesInPhonesIndexView
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.3-rtm-32065")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MyFirstMVC.Models.Category", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name");

                    b.Property<int?>("ParentCategoryId");

                    b.HasKey("Id");

                    b.HasIndex("ParentCategoryId");

                    b.ToTable("Categories");

                    b.HasData(
                        new { Id = 1, Name = "IOS" },
                        new { Id = 2, Name = "Android" },
                        new { Id = 3, Name = "Knopochnii" }
                    );
                });

            modelBuilder.Entity("MyFirstMVC.Models.Company", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Companies");

                    b.HasData(
                        new { Id = 1, Name = "Apple" },
                        new { Id = 2, Name = "Samsung" },
                        new { Id = 3, Name = "Nokia" },
                        new { Id = 4, Name = "Xiaomi" },
                        new { Id = 5, Name = "Meizu" },
                        new { Id = 6, Name = "Sony" }
                    );
                });

            modelBuilder.Entity("MyFirstMVC.Models.Currency", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CurrencyCode");

                    b.Property<string>("CurrencyName");

                    b.Property<double>("CurrencyRate");

                    b.HasKey("Id");

                    b.ToTable("Currencies");

                    b.HasData(
                        new { Id = "1", CurrencyCode = "RUB", CurrencyName = "Рубль", CurrencyRate = 57.0 },
                        new { Id = "2", CurrencyCode = "KGS", CurrencyName = "Сом", CurrencyRate = 68.0 }
                    );
                });

            modelBuilder.Entity("MyFirstMVC.Models.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Addres");

                    b.Property<string>("ContactPhone");

                    b.Property<int>("PhoneId");

                    b.Property<string>("User");

                    b.HasKey("Id");

                    b.HasIndex("PhoneId");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("MyFirstMVC.Models.Phone", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CategoryId");

                    b.Property<int>("CompanyId");

                    b.Property<string>("Name");

                    b.Property<double>("Price");

                    b.HasKey("Id");

                    b.HasIndex("CategoryId");

                    b.HasIndex("CompanyId");

                    b.ToTable("Phones");

                    b.HasData(
                        new { Id = 1, CategoryId = 1, CompanyId = 1, Name = "Iphone 7", Price = 700.0 },
                        new { Id = 2, CategoryId = 2, CompanyId = 6, Name = "Xperia X", Price = 120.0 },
                        new { Id = 3, CategoryId = 2, CompanyId = 2, Name = "Galaxy S6", Price = 600.0 },
                        new { Id = 4, CategoryId = 2, CompanyId = 5, Name = "M6T", Price = 200.0 },
                        new { Id = 5, CategoryId = 2, CompanyId = 4, Name = "Mi 8", Price = 450.0 },
                        new { Id = 6, CategoryId = 3, CompanyId = 3, Name = "230 Dual Sim", Price = 90.0 }
                    );
                });

            modelBuilder.Entity("MyFirstMVC.Models.PhoneOnStock", b =>
                {
                    b.Property<int>("PhoneId");

                    b.Property<int>("StockId");

                    b.Property<int>("Quantity");

                    b.HasKey("PhoneId", "StockId");

                    b.HasIndex("StockId");

                    b.ToTable("PhoneOnStocks");
                });

            modelBuilder.Entity("MyFirstMVC.Models.Stock", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Stocks");

                    b.HasData(
                        new { Id = 1, Name = "Stock 1" },
                        new { Id = 2, Name = "Stock 2" }
                    );
                });

            modelBuilder.Entity("MyFirstMVC.Models.Category", b =>
                {
                    b.HasOne("MyFirstMVC.Models.Category", "ParentCategory")
                        .WithMany("SubCategories")
                        .HasForeignKey("ParentCategoryId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("MyFirstMVC.Models.Order", b =>
                {
                    b.HasOne("MyFirstMVC.Models.Phone", "Phone")
                        .WithMany("Orders")
                        .HasForeignKey("PhoneId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("MyFirstMVC.Models.Phone", b =>
                {
                    b.HasOne("MyFirstMVC.Models.Category", "Category")
                        .WithMany("Phones")
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Restrict);

                    b.HasOne("MyFirstMVC.Models.Company", "Company")
                        .WithMany("Phones")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            modelBuilder.Entity("MyFirstMVC.Models.PhoneOnStock", b =>
                {
                    b.HasOne("MyFirstMVC.Models.Phone", "Phone")
                        .WithMany("Stocks")
                        .HasForeignKey("PhoneId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("MyFirstMVC.Models.Stock", "Stock")
                        .WithMany("Phones")
                        .HasForeignKey("StockId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}

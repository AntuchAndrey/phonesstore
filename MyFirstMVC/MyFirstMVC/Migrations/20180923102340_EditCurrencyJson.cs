﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyFirstMVC.Migrations
{
    public partial class EditCurrencyJson : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: "1",
                column: "CurrencyName",
                value: "Рубль");

            migrationBuilder.UpdateData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: "2",
                column: "CurrencyName",
                value: "Сом");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: "1",
                column: "CurrencyName",
                value: "�����");

            migrationBuilder.UpdateData(
                table: "Currency",
                keyColumn: "Id",
                keyValue: "2",
                column: "CurrencyName",
                value: "���");
        }
    }
}
